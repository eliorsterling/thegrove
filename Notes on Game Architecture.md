[Blender Game Engine](https://docs.blender.org/manual/en/2.79/game_engine/index.html)

[Google Voice Builder](https://github.com/google/voice-builder)

```
 User trains their own voice, then that voice can be downloaded by other participants and used in-game as the text-to-speech voice for that person's avatar. This can allow voice communication when there is only bandwidth for text between locations. (eg space, Antarctica)
```

[Motion Capture](https://chordata.cc/)

```
  We can use Motion Capture software from Chordata, combined with UPBGE (aka the new fork of Blender Game Engine), to create the basic library of character animations.

  At some point in the future, we hope that simple motion capture can be used to send movement information across the network so that avatars move similarly to each person. (eg when sitting at an in-game table and talking to each other)
```
