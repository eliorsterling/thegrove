# Use of NASA Research #

## Effective Working Crews In Remote Work and Isolation ##
The research of Noshir Contractor, PhD into creating effective teams and mitigating challenges in those teams can be used both in choosing the correct therapeutic teams for individual clients and their families and for creating mental health support plans for improving effectiveness in a work environment. 

### CREWS ###
https://sonic.northwestern.edu/projects-2/crews/

### Relational Analytics for Predicting Effective Crews ###

Lindsay Larson, Harrison Wojcik, Ilya Gokhman, Leslie DeChurch, Suzanne Bell, Noshir Contractor, Team performance in space crews: Houston, we have a teamwork problem,
Acta Astronautica, Volume 161, 2019, Pages 108-114,ISSN 0094-5765,
https://doi.org/10.1016/j.actaastro.2019.04.052.
(http://www.sciencedirect.com/science/article/pii/S0094576519301547)

### Mitigating the Effects of Communication Delay ###
Specific measures will be taken to reduce the delay in communication across long distances in order to mitigate the effects of communication delay on team dynamics as well as other social dynamics within the program. This is related to research cited in the poster at http://sonic.northwestern.edu/wp-content/uploads/2017/01/Ashley-IWS-2017.v6-SM.pdf
under the heading of "2. Communication Delay".

While full audio and video capabilities are designed into the Program for those situations where users have access to high bandwith connections, fallback options to reduce the bandwith use will take advantage of the abilities of the game engine to make the experience feel as similar to normal realtime communiation as possible. 

In addition, the repetetive nature of ongoing appointments and experiences in the game environment will help to mitigate the effects of physical separation leading to different understandings of the tasks at hand. (ibid, Discussion section.)

### Clowning Around ###
From https://www.nasa.gov/feature/nasa-studies-team-dynamics-in-antarctica
NASA research, including that mentioned above, has shown that humor and play help increase the resilience and ability of isolated teams to work together. The game developed by the Therapy Crossing team will take advantage of this dynamic by creating fun interactions and utilizing humor as the tools in the serious work of mental and behavioral health support.


## Assessment and Measurement Tools ##
Among the assessment tools used by The Grove, we will be using some tools developed by NASA. We have already talked about the CREWS project at HERA, which includes assessment to determine the best combination of participants for a team. We will also be using NASA designed tools for behavioral and emotional assessments.

## Behavioral Core Measures ##
This tool was created for NASA astronauts, and can be used for other people living and working in isolated situations.
https://www.nasa.gov/mission_pages/station/research/experiments/explorer/Investigation.html?#id=7537

## Characterization of Psychological Risk, Overlap with Physical Health, and Associated Performance in Isolated Confined Extreme (ICE) Envioronments ##
https://taskbook.nasaprs.com/tbp/index.cfm?action=public_query_taskbook_content&TASKID=12495

## Psychomotor Vigilance Test ##
https://data.nasa.gov/dataset/Psychomotor-Vigilance-Test-PVT-on-ISS/s6vw-3v2s


